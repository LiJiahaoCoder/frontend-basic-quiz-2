import Education from './Education';

export default class Person {
  constructor(name, age, description, educations) {
    this._name = name;
    this._age = age;
    this._description = description;
    this._educations = educations.map(element => {
      const { year, title, description } = element;
      return new Education(year, title, description);
    });
  }

  getName() {
    return this._name;
  }
  getAge() {
    return this._age;
  }
  getDescription() {
    return this._description;
  }
  getEducations() {
    return this._educations;
  }
}
