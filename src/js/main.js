// utils
import fetchData from './util/fetchData';
import render from './util/render';
// components
import Person from './Person';

fetchData
  .then(result => {
    const { name, age, description, educations } = result;
    const person = new Person(name, age, description, educations);
    render(person);
  })
  .catch(error => new Error(error));
