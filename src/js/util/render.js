import $ from 'jquery';

export default function render(person) {
  renderHeader(person.getName(), person.getAge());
  renderBody(person.getDescription(), person.getEducations());
}

function renderHeader(name, age) {
  $('#name').html(name);
  $('#age').html(age);
}

function renderBody(description, educations) {
  renderDescription(description);
  renderEducations(educations);
}

function renderDescription(description) {
  $('#description').html(description);
}

function renderEducations(educations) {
  const eduactionsEle = $('#educations');
  educations.forEach(element => {
    eduactionsEle.append(`
      <li class='education'>
        <span class='edu-year'>${element.getYear()}</span>
        <section class='edu-intro'>
          <p class='edu-title'>${element.getTitle()}</p>
          <p class='edu-description'>${element.getDescription()}</p>
        </section>
      </li>
    `);
  });
}
