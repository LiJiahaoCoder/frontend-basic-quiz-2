import { URL } from './data';

const fetchData = fetch(URL).then(result => result.json());

export default fetchData;
